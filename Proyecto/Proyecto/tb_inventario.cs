﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    public class tb_inventario
    {
        public int fk_platos { get; set; }
        public string nombre { get; set; }
        public decimal precio { get; set; }
        public string descripcion { get; set; }

        public tb_inventario()
        {

        }

        public tb_inventario(int fk_platos,string nombre, decimal precio, string descripcion)
        {
            this.fk_platos = fk_platos;
            this.nombre = nombre;
            this.precio = precio;
            this.descripcion = descripcion;
        }
    }
}
