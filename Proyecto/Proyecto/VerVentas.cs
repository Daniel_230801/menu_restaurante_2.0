﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto
{
    public partial class VerVentas : Form
    {
        public VerVentas()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MENU_GERENTE menug = new MENU_GERENTE();
            menug.Show();
            this.Dispose();
        }

        private void VerVentas_Load(object sender, EventArgs e)
        {
            dgv.DataSource = funciones.ver();
        }
    }
}
