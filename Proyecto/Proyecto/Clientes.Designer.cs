﻿namespace Proyecto
{
    partial class Menu_Cliemte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Agrager_Clientes = new System.Windows.Forms.Button();
            this.Regresar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(494, 363);
            this.dataGridView1.TabIndex = 0;
            // 
            // Agrager_Clientes
            // 
            this.Agrager_Clientes.Location = new System.Drawing.Point(49, 417);
            this.Agrager_Clientes.Name = "Agrager_Clientes";
            this.Agrager_Clientes.Size = new System.Drawing.Size(130, 38);
            this.Agrager_Clientes.TabIndex = 8;
            this.Agrager_Clientes.Text = "Agregar nuevo cliente";
            this.Agrager_Clientes.UseVisualStyleBackColor = true;
            // 
            // Regresar
            // 
            this.Regresar.Location = new System.Drawing.Point(370, 417);
            this.Regresar.Name = "Regresar";
            this.Regresar.Size = new System.Drawing.Size(78, 32);
            this.Regresar.TabIndex = 9;
            this.Regresar.Text = "Regresar";
            this.Regresar.UseVisualStyleBackColor = true;
            this.Regresar.Click += new System.EventHandler(this.Regresar_Click);
            // 
            // Menu_Cliemte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 483);
            this.Controls.Add(this.Regresar);
            this.Controls.Add(this.Agrager_Clientes);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu_Cliemte";
            this.Text = "Clientes";
            this.Load += new System.EventHandler(this.Menu_Cliemte_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Agrager_Clientes;
        private System.Windows.Forms.Button Regresar;
    }
}