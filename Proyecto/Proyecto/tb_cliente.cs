﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    public class tb_cliente
    {
        public string nit { get; set; }
        public string nombre { get;set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string comentario { get; set; }

        public tb_cliente()
        {

        }

        public tb_cliente(string nit,string nombre,string direccion,string telefono,string comentario)
        {
            this.nit = nit;
            this.nombre = nombre;
            this.direccion = direccion;
            this.telefono = telefono;
            this.comentario = comentario;
        }
    }
}
