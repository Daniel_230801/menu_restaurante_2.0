﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Proyecto
{
    class funciones
    {
        public static int agg(tb_inventario add)
        {
            int retorno = 0;
            MySqlCommand comando = new MySqlCommand(string.Format("insert into tb_inventario_menu(fk_platos,nombre,precio_unitario,descripcion)value ({0},'{1}',{2},'{3}')", add.fk_platos,add.nombre, add.precio, add.descripcion), Conexion.ObtenerConexion());
            retorno = comando.ExecuteNonQuery();
            return retorno;
        }

        public static List<tb_inventario> mostrar()
        {
            List<tb_inventario> lista = new List<tb_inventario>();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * From tb_inventario_menu"), Conexion.ObtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                tb_inventario aña = new tb_inventario();
                aña.fk_platos = reader.GetInt32(1);
                aña.nombre = reader.GetString(2);
                aña.precio = reader.GetDecimal(3);
                aña.descripcion = reader.GetString(4);
                lista.Add(aña);
            }
            return lista;
        }

        public static int mostrar(tb_ventas add)
        {
            int retorno = 0;
            MySqlCommand comando2 = new MySqlCommand(string.Format("insert into tb_ventas(id_inventario_menu,nombre, nombreproducto, nit,cantidad, total)value ({0},'{1}','{2}','{3}',{4},{5})", add.idplato,add.nombrecliente, add.nombreproducto,add.nit, add.cantidad,add.total), Conexion.ObtenerConexion());
            retorno = comando2.ExecuteNonQuery();
            return retorno;
        }

        public static List<tb_inventario> Buscar(int fk_platos)
        {
            List<tb_inventario> ListaBuscar = new List<tb_inventario>();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * from tb_inventario_menu where fk_platos={0}", fk_platos), Conexion.ObtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                tb_inventario a = new tb_inventario();
                a.fk_platos = reader.GetInt32(1);
                a.nombre = reader.GetString(2);
                a.precio = reader.GetDecimal(3);
                a.descripcion = reader.GetString(4);
                ListaBuscar.Add(a);
            }
            return ListaBuscar;
        }

        /*
        public static tb_inventario seleccionarventa(int fk_platos)
        {
            tb_inventario seleccionar = new tb_inventario();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * from tb_inventario_menu where fk_platos={0}", fk_platos), Conexion.ObtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                tb_inventario a = new tb_inventario();
                a.fk_platos = reader.GetInt32(1);
                a.nombre = reader.GetString(2);
                a.precio = reader.GetDecimal(3);
                a.descripcion = reader.GetString(4);
            }
            return seleccionar;
        }
        */

        public static List<tb_ventas> ver()
        {
            List<tb_ventas> lista = new List<tb_ventas>();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * From tb_ventas"), Conexion.ObtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                tb_ventas aña = new tb_ventas();
                aña.idplato = reader.GetInt32(1);
                aña.nombrecliente = reader.GetString(3);
                aña.nit =reader.GetString(4);
                aña.nombreproducto = reader.GetString(2);
                aña.cantidad = reader.GetInt32(5);
                aña.total = reader.GetDecimal(6);
                lista.Add(aña);
            }
            return lista;
        }

        public void seleccionador(ComboBox cb)
        {
            cb.Items.Clear();
            Conexion.ObtenerConexion();
            MySqlCommand cmd = new MySqlCommand(String.Format("Select *from tb_inventario_menu"), Conexion.ObtenerConexion());
            MySqlDataReader dr = cmd.ExecuteReader();
            Conexion.CerrarConexion();
            while (dr.Read())
            {
                cb.Items.Add(dr[2].ToString());
            }
            Conexion.CerrarConexion();
            cb.Items.Insert(0, "--- Seleccione un producto ---");
            cb.SelectedIndex = 0;
        }

        public string[] captarinformacion(string nombre)
        {
            Conexion.ObtenerConexion();
            MySqlCommand cmd = new MySqlCommand(String.Format("Select *from tb_inventario_menu where nombre='" + nombre + "'"), Conexion.ObtenerConexion());
            MySqlDataReader dr = cmd.ExecuteReader();
            string[] resultado = null;
            while (dr.Read())
            {
                string[] valores =
                {
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                };
                resultado = valores;
            }
            Conexion.CerrarConexion();
            return resultado;
        }

        public static int agg2(tb_cliente add2)
        {
            int retorno2 = 0;
            MySqlCommand comando2 = new MySqlCommand(string.Format("insert into tb_cliente(nit,nombre,direccion,telefono,comentario)value ('{0}','{1}','{2}','{3}','{4}')", add2.nit, add2.nombre, add2.direccion, add2.telefono, add2.comentario), Conexion.ObtenerConexion());
            retorno2 = comando2.ExecuteNonQuery();
            return retorno2;
        }


        /*
        public static List<tb_cliente> mostrarcliente()
        {
            List<tb_cliente> lista = new List<tb_cliente>();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * from tb_cliente"), Conexion.ObtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                tb_cliente añadir = new tb_cliente();
                añadir.nit = reader.GetString(1);
                añadir.nombre = reader.GetString(2);
                añadir.direccion = reader.GetString(3);
                añadir.telefono = reader.GetString(4);
                añadir.comentario = reader.GetString(5);
                lista.Add(añadir);
            }
            return lista;
        }
        */
        
        public static List<tb_cliente> buscarcliente(string nit)
        {
            List<tb_cliente> listadebuscar = new List<tb_cliente>();
            MySqlCommand comando = new MySqlCommand(String.Format("Select * from tb_cliente where nit='{0}'", nit), Conexion.ObtenerConexion());
            MySqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                tb_cliente c = new tb_cliente();
                c.nit = reader.GetString(1);
                c.nombre = reader.GetString(2);
                c.direccion = reader.GetString(3);
                c.telefono = reader.GetString(4);
                listadebuscar.Add(c);
            }
            return listadebuscar;
        }

        



    }
}
