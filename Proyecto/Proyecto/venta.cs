﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Proyecto
    
{
    public partial class venta : Form
    {
      
        funciones combo = new funciones();
        public tb_inventario ventaseleccionada { get; set; }
        public tb_inventario ventaactual { get; set; }
        public venta()
        {
            InitializeComponent();
            combo.seleccionador(cb:combito);
            //Con este codigo evitamos la edicion del combobox
            combito.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MENU_GERENTE menug = new MENU_GERENTE();
            menug.Show();
            this.Dispose();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void venta_Load(object sender, EventArgs e)
        {
            
        }

       
        private void button5_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtnit.Text))
            {
                MessageBox.Show("Campos vacios");
            }
            else
            {
                dgv2.DataSource = funciones.buscarcliente(txtnit.Text);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtidplato.Text=="" || txtNombreProducto.Text == "" || txtcantidad.Text == "" || txtnit.Text == "" || txttotal.Text=="" || txtnombrecliente.Text=="")
            {
                MessageBox.Show("Datos vacios");
            }
            else
            {
                Conexion.ObtenerConexion();
                tb_ventas agregar = new tb_ventas();
                agregar.idplato = Convert.ToInt32(txtidplato.Text);
                agregar.nombrecliente = txtnombrecliente.Text;
                agregar.total = Convert.ToDecimal(txttotal.Text);
                agregar.nombreproducto = txtNombreProducto.Text;
                agregar.cantidad = Int32.Parse((txtcantidad.Text));
                agregar.nit = txtnit.Text;
                int retorno = funciones.mostrar(agregar);
                dgv.DataSource = funciones.ver();

                if (retorno > 0)
                {
                    MessageBox.Show("Venta agregada");
                    txtNombreProducto.Text = "";
                    txtidplato.Text = "";
                    txtcantidad.Text = "";
                    txtcantidad.Text = "";
                    txttotal.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo agregar la venta");
                }
            }

           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            VerVentas v = new VerVentas();
            v.Show();
            this.Dispose();
        }

        private void combito_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combito.SelectedIndex > 0)
            {
                string[] valores = combo.captarinformacion(combito.Text);
                txtidplato.Text = valores[0];
                txtNombreProducto.Text = valores[1];
                txttotal.Text = valores[2];
            }
        }

        private void dgv2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtnombrecliente.Text = dgv2.CurrentRow.Cells[1].Value.ToString();
            }
            catch
            {

            }
        }
    }
}
