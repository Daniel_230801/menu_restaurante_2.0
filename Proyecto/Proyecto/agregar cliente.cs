﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto
{
    public partial class agregar_cliente : Form
    {
        public agregar_cliente()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtnit.Text == "" || txtnombre.Text == "" || txtdireccion.Text == "" || txttelefono.Text == "" || txtcomentario.Text == "")
            {
                MessageBox.Show("Datos vacios");
            }
            else
            {
                Conexion.ObtenerConexion();
                tb_cliente agregar2 = new tb_cliente();
                agregar2.nit = txtnit.Text;
                agregar2.nombre = txtnombre.Text;
                agregar2.direccion = txtdireccion.Text;
                agregar2.telefono = txttelefono.Text;
                agregar2.comentario = txtcomentario.Text;
                int retorno2 = funciones.agg2(agregar2);
                if (retorno2 > 0)
                {
                    MessageBox.Show("Cliente agregado");
                    txtnit.Text = "";
                    txtnombre.Text = "";
                    txtdireccion.Text = "";
                    txttelefono.Text = "";
                    txtcomentario.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo añadir el cliente");
                }
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MENU_GERENTE MG = new MENU_GERENTE();
            MG.Show();
            this.Dispose();
        }
    }
}
