﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Proyecto
{
    public class tb_ventas
    {
        public int idplato { get; set; }
        public string nombreproducto { get; set; }
        public decimal total { get; set; }
        public int cantidad { get; set; }
        public string nit { get; set; }
        public string nombrecliente { get; set; }
            
        public tb_ventas()
        {
        }
        public tb_ventas(int idplato,string nombrecliente,string nombreproducto,int cantidad, string nit,decimal totl)
        {
            this.idplato = idplato;
            this.nombrecliente = nombrecliente;
            this.nombreproducto = nombreproducto;
            this.cantidad = cantidad;
            this.nit = nit;
            this.total = total;
           
        }



    }
}
