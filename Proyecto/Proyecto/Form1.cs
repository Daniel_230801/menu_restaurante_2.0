﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto
{
    public partial class INICIO : Form
    {
        public INICIO()
        {
            InitializeComponent();
            Conexion.ObtenerConexion();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Conexion.CerrarConexion();
            Application.Exit();
            
        }

        private void INICIAR_Click(object sender, EventArgs e)
        {
            if (txt_usuario.Text == "" && txt_contraseña.Text == "")
            {
                MessageBox.Show("No ingreso usuario ni contraseña!");
            }
            else if (txt_usuario.Text == "")
            {
                MessageBox.Show("No ingreso Usuario!");
            }
            else if (txt_contraseña.Text == "")
            {
                MessageBox.Show("No ingreso Contraseña!");
            }
            else
            {
                MySqlCommand cmsql = new MySqlCommand("SELECT usuario, permisos FROM tb_usuarios WHERE usuario = '" + txt_usuario.Text + "' AND password = '" + txt_contraseña.Text + "'", Conexion.ObtenerConexion());
                MySqlDataAdapter msqlad = new MySqlDataAdapter(cmsql);
                DataTable dt = new DataTable();
                msqlad.Fill(dt);
                if (dt.Rows.Count == 1) // comprobacion que exista con que sea un dato en la consulta...
                {
                    if (dt.Rows[0][1].ToString() == "gerente")
                    {
                        this.Hide();
                        bienvenida welcomer = new bienvenida();
                        welcomer.ShowDialog();
                        MENU_GERENTE f1 = new MENU_GERENTE();
                        f1.Show();
                        this.Hide();
                    }
                    else if (dt.Rows[0][1].ToString() == "empleado")
                    {
                        bienvenida welcomer = new bienvenida();
                        welcomer.ShowDialog();
                        Menu_Empleado f1 = new Menu_Empleado();
                        f1.Show();
                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("Las credenciales que ingreso no son correctas, porfavor vuelva a intentarlo!");
                }
            }
        }

        private void INICIO_Load(object sender, EventArgs e)
        {

        }
    }
}
