﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto
{
    public partial class Empleados : Form
    {
        MySqlDataAdapter msqlad;
        MySqlCommand cmsql;
        DataTable dt;
        int ID = 0;
        String usuario;
        String password;
        String permisos;    
        public Empleados()
        {
            InitializeComponent();
            Rellenar();
        }

        public void Rellenar()
        {
            cmsql = new MySqlCommand("SELECT * FROM tb_usuarios", Conexion.ObtenerConexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            dataGridView1.DataSource = dt;
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            usuario = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            password = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            permisos = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void regresar_Click(object sender, EventArgs e)
        {
            MENU_GERENTE menug = new MENU_GERENTE();
            menug.Show();
            this.Dispose();
        }

        private void Empleados_Load(object sender, EventArgs e)
        {

        }
    }
}
