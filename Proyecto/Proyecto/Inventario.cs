﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Proyecto
{
    public partial class Inventario : Form
    {
        MySqlDataAdapter msqlad;
        MySqlCommand cmsql;
        DataTable dt;
        int ID = 0;
        String nombre;
        String precioUnitario;
        String descripción;

        public Inventario()
        {
            InitializeComponent();
            Rellenar();
        }

        public void Rellenar()
        {
            cmsql = new MySqlCommand("SELECT * FROM tb_inventario_menu", Conexion.ObtenerConexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            dgv.DataSource = dt;
        }
        private void Inventario_Load(object sender, EventArgs e)
        {
            dgv.DataSource = funciones.mostrar();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = Convert.ToInt32(dgv.Rows[e.RowIndex].Cells[0].Value.ToString());
            nombre = dgv.Rows[e.RowIndex].Cells[1].Value.ToString();
            precioUnitario = dgv.Rows[e.RowIndex].Cells[2].Value.ToString();
            descripción = dgv.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void regresar_Click(object sender, EventArgs e)
        {
            MENU_GERENTE menug = new MENU_GERENTE();
            menug.Show();
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtfkidplatos.Text == "" || txtnombre.Text == "" || txtprecio.Text == "" || txtdescripcion.Text == "")
            {
                MessageBox.Show("Datos vacios");
            }
            else
            {
                Conexion.ObtenerConexion();
                tb_inventario agregar = new tb_inventario();
                agregar.fk_platos = Int32.Parse((txtfkidplatos.Text));
                agregar.nombre = txtnombre.Text;   
                agregar.precio = decimal.Parse((txtprecio.Text));
                agregar.descripcion = txtdescripcion.Text;
                int retorno = funciones.agg(agregar);

                if (retorno > 0)
                {
                    MessageBox.Show("Plato agregado correctamente");
                    txtfkidplatos.Text = "";
                    txtnombre.Text = "";
                    txtprecio.Text = "";
                    txtdescripcion.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo agregar el plato");
                }
                
                
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            dgv.DataSource = funciones.mostrar();
        }

        
       
    }
}
